package com.gatehill.docker.ports.model;

import java.util.Arrays;
import java.util.List;

/**
 * @author Pete
 */
public class ContainerWithPort {
    private final String id;
    private final String[] names;
    private final List<Integer> ports;

    public ContainerWithPort(String id, String[] names, List<Integer> ports) {
        this.id = id;
        this.names = names;
        this.ports = ports;
    }

    @Override
    public String toString() {
        return "ContainerWithPort{" +
                "id='" + id + '\'' +
                ", names=" + Arrays.toString(names) +
                ", ports=" + ports +
                '}';
    }

    public String getId() {
        return id;
    }

    public String[] getNames() {
        return names;
    }

    public List<Integer> getPorts() {
        return ports;
    }
}
