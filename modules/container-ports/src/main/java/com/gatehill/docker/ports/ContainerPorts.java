package com.gatehill.docker.ports;

import ch.qos.logback.classic.Level;
import com.gatehill.docker.ports.model.ContainerWithPort;
import com.github.dockerjava.api.DockerClient;
import com.github.dockerjava.api.command.ListContainersCmd;
import com.github.dockerjava.api.model.Container;
import com.github.dockerjava.api.model.Event;
import com.github.dockerjava.api.model.Filters;
import com.github.dockerjava.core.DockerClientBuilder;
import com.github.dockerjava.core.DockerClientConfig;
import com.github.dockerjava.core.command.EventsResultCallback;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sharneng.net.NetUtils;
import com.sharneng.net.portforward.Listener;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author Pete
 */
public class ContainerPorts {
    private static final Logger LOGGER = LoggerFactory.getLogger(ContainerPorts.class);
    private static final String DEFAULT_ENGINE_HOST_OR_IP = "192.168.99.100";

    private Map<String, List<Listener>> listeners = Maps.newConcurrentMap();
    private DockerClient dockerClient;

    @Option(name = "--help", aliases = { "-h" }, usage = "Display usage only", forbids = { "-e", "-m" })
    private boolean displayHelp;

    @Option(name = "--debug", usage = "Log at DEBUG level")
    private boolean logDebug;

    @Option(name = "--engine", aliases = { "-e" }, usage = "The IP or hostname of the Docker engine")
    private String engineHostOrIp;

    @Option(name = "--machine", aliases = { "-m" }, usage = "The Docker Machine name")
    private String dockerMachineName = "dev";
    
    public static void main(String... args) {
        final ContainerPorts containerPorts = new ContainerPorts();
        final CmdLineParser parser = new CmdLineParser(containerPorts);
        try {
            parser.parseArgument(args);

            // override log level
            if (containerPorts.logDebug) {
                final Logger rootLogger = LoggerFactory.getLogger(Logger.ROOT_LOGGER_NAME);
                if (rootLogger instanceof ch.qos.logback.classic.Logger) {
                    ((ch.qos.logback.classic.Logger) rootLogger).setLevel(Level.DEBUG);
                }
            }

            // begin execution
            if (containerPorts.displayHelp) {
                parser.printUsage(System.out);
            } else {
                containerPorts.run();
            }

        } catch (CmdLineException e) {
            // handling of wrong arguments
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
        }
    }

    private void run() {
        // TODO determine IP from Docker Machine name
        if (Strings.isNullOrEmpty(engineHostOrIp)) {
            LOGGER.info("No engine host/IP provided - guessing " + DEFAULT_ENGINE_HOST_OR_IP);
            engineHostOrIp = DEFAULT_ENGINE_HOST_OR_IP;
        }

        final DockerClient docker = getDockerClient(engineHostOrIp);
        listenForEvents(docker);
        processRunningContainers(docker);
    }

    private void processRunningContainers(DockerClient docker) {
        LOGGER.info("Syncing initial state with Docker daemon...");
        final List<ContainerWithPort> containers = fetchContainersWithPorts(docker, null);

        for (ContainerWithPort container : containers) {
            forwardAllContainerPorts(engineHostOrIp, container);
        }
    }

    private void listenForEvents(DockerClient docker) {
        LOGGER.info("Listening for Docker events...");

        docker.eventsCmd()
                .exec(new EventsResultCallback() {
                    @Override
                    public void onNext(Event item) {
                        super.onNext(item);

                        if (item.getStatus().equals("start")) {
                            LOGGER.info("Container started - checking for ports");

                            ContainerWithPort container = fetchContainer(docker, item.getId());
                            if (null != container) {
                                forwardAllContainerPorts(engineHostOrIp, container);
                            }
                        }

                        if (item.getStatus().equals("die")) {
                            LOGGER.info("Container died - closing ports");
                            closeContainerPorts(item.getId());
                        }
                    }
                });
    }

    private void closeContainerPorts(String containerId) {
        List<Listener> containerListeners = listeners.get(containerId);
        if (null != containerListeners) {
            LOGGER.info("Closing {} ports for container {}", containerListeners.size(), containerId);

            Iterator<Listener> it = containerListeners.iterator();
            while (it.hasNext()) {
                final Listener listener = it.next();
                LOGGER.debug("Closing port {} for container {}", listener.getPort(), containerId);

                listener.close();
                it.remove();
            }

        } else {
            LOGGER.debug("No ports to close for container {}", containerId);
        }
    }

    private ContainerWithPort fetchContainer(DockerClient docker, String containerId) {
        final List<ContainerWithPort> containers =
                fetchContainersWithPorts(docker, containerId);

        if (containers.size() > 0) {
            return containers.get(0);
        } else {
            return null;
        }
    }

    private void forwardAllContainerPorts(String engineHostOrIp, ContainerWithPort container) {
        for (Integer port : container.getPorts()) {
            forwardPort(container, engineHostOrIp, port);
        }
    }

    private void forwardPort(ContainerWithPort container, String remoteIp, Integer publicPort) {
        try {
            // TODO check whether port is already forwarded (and whether it's the same container)

            LOGGER.debug("Forwarding container {} port {} to {}", container.getId(), publicPort, remoteIp);

            final Listener listener = new Listener(NetUtils.parseInetSocketAddress("localhost:" + publicPort),
                    NetUtils.parseInetSocketAddress(remoteIp + ":" + publicPort));

            // hold against container ID
            List<Listener> containerListeners = listeners.get(container.getId());
            if (null == containerListeners) {
                containerListeners = Lists.newArrayList();
                listeners.put(container.getId(), containerListeners);
            }
            containerListeners.add(listener);

            new Thread(listener).start();

        } catch (IOException e) {
            LOGGER.error(String.format("Error forwarding port %s to %s", publicPort, remoteIp), e);
        }
    }

    private List<ContainerWithPort> fetchContainersWithPorts(DockerClient docker, String containerId) {
        final ListContainersCmd listContainersCmd = docker.listContainersCmd();

        // restrict to ID
        // NOTE: this is currently broken in the docker-java library
        if (null != containerId) {
            listContainersCmd.withFilters(new Filters().withContainers(containerId));
        }

        final List<Container> containers = listContainersCmd.exec();

        final List<ContainerWithPort> containersWithPort = containers.stream()
                .filter(c -> c.ports.length > 0)

                        // this filter is required due to a bug in the docker-java library
                .filter(c -> (null == containerId || c.getId().equals(containerId)))

                .map(c -> new ContainerWithPort(c.getId(), c.getNames(), getPorts(c)))
                .collect(Collectors.toList());

        LOGGER.info("Found {} containers exposing ports: {}", containersWithPort.size(), containersWithPort);
        return containersWithPort;
    }

    /**
     * @param container
     * @return the non-<code>null</code> public ports from the List
     */
    private List<Integer> getPorts(Container container) {
        final List<Integer> ports = Lists.newArrayList();

        for (Container.Port port : container.ports) {
            if (null != port.getPublicPort()) {
                ports.add(port.getPublicPort());
            } else {
                LOGGER.debug("Skipping null public port: {}", port);
            }
        }

        return  ports;
    }

    private DockerClient getDockerClient(String engineHostOrIp) {
        if (null == dockerClient) {
            DockerClientConfig config = DockerClientConfig.createDefaultConfigBuilder()
                    .withVersion("1.19")
                    .withUri("https://" + engineHostOrIp + ":2376")
                            //.withUsername("dockeruser")
                            //.withPassword("ilovedocker")
                            //.withEmail("dockeruser@github.com")
                            //.withServerAddress("https://index.docker.io/v1/")
                    .withDockerCertPath(System.getProperty("user.home") + "/.docker/machine/machines/" + dockerMachineName)
                    .build();

            dockerClient = DockerClientBuilder.getInstance(config).build();
        }

        return dockerClient;
    }
}
