# Container Ports: Forward TCP/IP ports from Docker containers to localhost

[![Build Status](https://drone.io/bitbucket.org/outofcoffee/container-ports/status.png)](https://drone.io/bitbucket.org/outofcoffee/container-ports/latest)

**WARNING: This is highly experimental and is likely to have many bugs**

## Why would I want this?

If you run *Docker Machine* on a non-Linux host, such as Mac OS X or Windows you might be tired
of typing ``docker-machine ip dev`` all the time.

This utility will map the publicly exposed ports from containers running on a given Docker Engine to localhost.

### An example

Say your Docker Engine runs on ``192.168.99.100``:

    $ docker-machine env dev
    192.168.99.100

Normally, if you run a container that exposes ports, say 8080, then you have to use the IP address of the Docker
Engine host VM to access them. For example:

    docker run -ti --rm -p 8080:8080 jboss/wildfly

...then you have to hit:

    http://192.168.99.100:8080

But what if the IP address of the Docker Engine host changes? You have to notice this and hit a different IP address.

### A better way
Instead of remembering IP addresses, Container Ports makes the ports accessible on localhost.
This means you can hit:

    http://localhost:8080

Simple! Container Ports also listens for start/stop of containers and opens and closes ports appropriately.

## Build

### Prerequisites:

* JDK 8+

### Build it

First pull the submodules:

    git submodule update --init --recursive

### Option A: One liner
If you don't want to go through the step by step instructions, you can use this one liner:

On *NIX and OS X:

    bash ./bin/setup-container-ports.sh

On Windows:

    bin\setup-container-ports.bat

### Option B: Step by step
Build the JAR:

    bash ./gradlew shadowJar

The JAR is built to the ``modules/container-ports/build/libs`` subdirectory.
To run it, we can use the convenience script in the ``bin`` directory.

On *NIX and OS X only: first, let's make it executable:

    cd bin
    chmod +x ./container-ports

Now you can do this:

    ./container-ports [options]

*Important:* It is strongly advisable to make the tool accessible on your ``PATH``.
To do this you can add the path to the ``bin`` folder to your ``PATH``
in your ``~/.bash_profile`` file. The 'one liner' option above will do this for you.

You're good to go. Check out the *Usage* section for options.

## Run

Once built, run as follows:

    container-ports

Tip: Increase logging with ``--debug`` flag.

## Usage

     --debug            : Log at DEBUG level
     --engine (-e) VAL  : The IP or hostname of the Docker engine
     --machine (-m) VAL : The Docker Machine name
     --help (-h)        : Display usage only

For example, here's how you would specify the engine and machine name:

    container-ports --engine 123.123.123.123 --machine dev

This example presumes you have a Docker Machine named ``dev`` and that it is running on IP ``123.123.123.123``.

# TODO
* Lots of tidy up
* Tests
* Inference of Docker Engine IP from Docker Machine name (use a command line argument or use the one found if only one)
* Allow specification of Docker daemon port

# Author
Written and maintained by Pete Cornish.

# Acknowledgements
* This project uses a fork of the ``sharneng/portforward`` project.
* The idea of listening for Docker events came from a discussion with Richard North.