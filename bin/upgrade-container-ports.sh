#!/bin/sh

set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_HOME="$( cd ${SCRIPT_DIR}/.. && pwd )"

# build from source
echo ""
echo "Cleaning project"
cd ${PROJECT_HOME}
sh ./gradlew clean

# pull latest
echo ""
echo "Pulling latest code"
git reset --hard HEAD
git checkout master
git pull

# invoke setup
cd ${SCRIPT_DIR}

export ADD_TO_BASH_PROFILE="n"
sh ./setup-container-ports.sh