@echo off

SET SCRIPT_DIR=%~dp0
SET MODULE_HOME=%SCRIPT_DIR%\..\modules\container-ports
SET JAR_NAME=container-ports-1.0-all
SET JAVA_OPTS=%JAVA_OPTS%

:: run the tool
java -jar %MODULE_HOME%\build\libs\%JAR_NAME%.jar %JAVA_OPTS% %*
