@echo off

SET SCRIPT_DIR=%~dp0
SET PROJECT_HOME=%SCRIPT_DIR%\..

:: build from source
echo.
echo Cleaning project
cd %PROJECT_HOME%
call gradlew.bat clean

:: pull latest
echo.
echo Pulling latest code
git reset --hard HEAD
git checkout master
git pull

:: invoke setup
cd %SCRIPT_DIR%
call setup-container-ports.bat