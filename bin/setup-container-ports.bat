@echo off

SET SCRIPT_DIR=%~dp0
SET PROJECT_HOME=%SCRIPT_DIR%\..

:: build from source
echo.
echo Building from source
cd %PROJECT_HOME%
call gradlew.bat clean shadowJar

echo.
echo It is strongly recommended that you add the following to your 'Path' variable:
echo %PROJECT_HOME%

echo.
echo Setup complete
