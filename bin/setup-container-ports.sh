#!/bin/sh

set -e

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJECT_HOME="$( cd ${SCRIPT_DIR}/.. && pwd )"

# build from source
echo ""
echo "Building from source"
cd ${PROJECT_HOME}
sh ./gradlew clean shadowJar

# set up script
echo ""
echo "Setting up executable"
chmod +x ${SCRIPT_DIR}/container-ports

if [[ "" == "${ADD_TO_BASH_PROFILE}" ]]; then
    echo ""
    echo "Add to ~./bash_profile? [y/n]"
    read ADD_TO_BASH_PROFILE
fi

if [[ "y" == "${ADD_TO_BASH_PROFILE}" ]]; then
    echo ""
    echo "Adding to ~/.bash_profile"

    echo "" >> ~/.bash_profile
    echo "" >> ~/.bash_profile
    echo "export CONTAINER_PORTS_HOME=${SCRIPT_DIR}" >> ~/.bash_profile
    echo "export PATH=\$PATH:\${CONTAINER_PORTS}" >> ~/.bash_profile

    echo "Done. You should run: source ~/.bash_profile"
fi

echo ""
echo "Setup complete"
